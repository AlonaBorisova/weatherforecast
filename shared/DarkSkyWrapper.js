const DarkSkyApi = require('dark-sky-api');
const config = require('../config').darkSky;

class DarkSkyService {
  constructor() {
    DarkSkyApi.apiKey = config.apiKey;
    DarkSkyApi.proxy = config.proxy;
    DarkSkyApi.units = config.units;
    DarkSkyApi.language = config.language;
    DarkSkyApi.postProcessor = (item) => {
      item.dateTime.locale('uk');
      item.day = item.dateTime.format('dddd');
      item.date = item.dateTime.format('YY-MM-DD');
      item.dateTime = item.dateTime.format('D MMMM');
      item.sunsetTime = item.sunsetDateTime.format('h:mm');
      item.sunriseTime = item.sunriseDateTime.format('h:mm');
      return item;
    };
  }

  _loadForecast(position) {
    const location = position || config.defaultLocation;
    return DarkSkyApi.loadForecast(location);
  }
  loadCurrentPosition(){
    return DarkSkyApi.loadPosition()
      .then(geoPosition => ({
        location: {
          latitude: geoPosition.latitude,
          longitude: geoPosition.longitude,
        },
        allowed: true
      }))
      .catch(err => {
        console.log(err);
        return {
          location: config.defaultLocation,
          allowed: false
        };
      });
  }
  getWeatherForecast(position, sendResult) {
    return this._loadForecast(position)
      .then(result => sendResult(result))
      .catch(err => console.log(err));
  }
}

module.exports = new DarkSkyService();
const defaults = {
  server: {
    port: process.env.PORT || 8001,
    host: process.env.HOST || '127.0.0.1',
  },
  darkSky: {
    apiKey: '75429b749307331344eb8850de05a869',
    proxy: true,
    units: 'si',
    language: 'uk',
    defaultLocation: {
      latitude: 48.4589523,
      longitude: 34.9878968,
    },
  },
};

module.exports = defaults;
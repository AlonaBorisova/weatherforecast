export default {
  '.cards-container': {
    display: 'grid',
    gridTemplateColumns: 'repeat(auto-fill, minmax(200px, 1fr))',
    gridTemplateRows: 'minmax(200px, 1fr) auto',
    gridRowGap: '20px',
    gridColumnGap: '20px',
    maxWidth: '860px',
    margin: '0 auto',
    minHeight: '100%',
    overflowX: 'hidden',
    textAlign: 'center',
  },
  '.summary': {
    gridColumn: '1 / -1',
    backgroundColor: '#0046b1',
    fontSize: '26px',
    color: 'white',
    borderRadius: '10px',
    padding: '15px 15px',
    border: '1px solid #0046b1',
  },
  '.temperature > span': {
    fontSize: '15px',
    paddingLeft: '20px',
  },
  '.temperature > span > span': {
    fontSize: '30px',
  },
  '.day-wrapper': {
    border: '2px solid #fff',
    background: '#0046b1',
    borderRadius: '10px',
    color: '#fff',
    padding: '10px',
  },
  'h3, h2, p': {
    margin: '10px 0',
  },
  '.day-summary': {
    padding: '20px 0 0',
  },
  '.details': {
    fontSize: '15px',
    textDecoration: 'underline',
    cursor: 'pointer',
    marginTop: '10px',
  },
  '.detailed-wrapper': {
    textAlign: 'left',
    fontSize: '15px',
    marginTop: '10px',
  },
  '.detailed-wrapper > div': {
     padding: '2px 0',
  },
  '.detailed-wrapper > div > span': {
    paddingRight: '15px',
  },
  '.loader, .not-found': {
    textAlign: 'center',
    marginTop: '50px',
    fontSize: '50px',
  },
};
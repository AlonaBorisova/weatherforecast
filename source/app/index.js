import React from 'react';
import { render } from 'react-dom';
import { createStore, applyMiddleware, compose } from 'redux';
import rootReducer from './reducers/index';
import epicMiddleware from './epics';
import Root from './Root';

const store = createStore(rootReducer, compose(applyMiddleware(epicMiddleware)));

render(<Root store={store} />,
  document.getElementById('root'));

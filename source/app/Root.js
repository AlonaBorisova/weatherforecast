import React from 'react';
import PropTypes from 'proptypes';
import { Provider } from 'react-redux';
import { StyleRoot } from 'radium';
import Router from './router/Router';

const Root = ({ store }) => (
  <Provider store={store}>
    <StyleRoot>
      <Router />
    </StyleRoot>
  </Provider>
);

Root.propTypes = {
  store: PropTypes.object.isRequired,
};

export default Root;
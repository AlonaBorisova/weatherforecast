import { createSelector } from 'reselect';
import { formatDaily, formatData } from '../helpers/Utils';

export default () => {
  const forecastDailySelector = (state) => formatDaily(state.forecast.daily);
  const forecastSelector = (state) => formatData(state.forecast);

  return createSelector(
    [forecastDailySelector, forecastSelector],
    (daily, generalInfo) => ({
      daily,
      generalInfo,
    }));
};


import { ICONS } from '../helpers/Icons';
import _ from 'lodash';

export function getSvgIconName(type) {
  const icons = {
    'clear-day': 'CLEAR_DAY',
    'clear-night': 'CLEAR_NIGHT',
    'partly-cloudy-day': 'PARTLY_CLOUDY_DAY',
    'partly-cloudy-night': 'PARTLY_CLOUDY_NIGHT',
    'cloudy': 'CLOUDY',
    'rain': 'RAIN',
    'sleet': 'SLEET',
    'snow': 'SNOW',
    'wind': 'WIND',
    'fog': 'FOG',
  };
  return icons[type];
}

export function formatTime(dateTime) {
  return new Date(dateTime).toLocaleTimeString('uk-UA');
}

export function formatDay(days) {
  return _.map(days, (day) => {
    return {
      temperatureLow: Math.floor(day.temperatureLow),
      temperatureHigh: Math.floor(day.temperatureHigh),
      apparentTemperatureHigh: Math.floor(day.apparentTemperatureHigh),
      apparentTemperatureLow: Math.floor(day.apparentTemperatureLow),
      cloudCover: day.cloudCover,
      date: day.date,
      dateTime: day.dateTime,
      day: day.day,
      humidity: day.humidity,
      icon: ICONS[getSvgIconName(day.icon)],
      summary: day.summary,
      sunriseTime: day.sunriseTime,
      sunsetTime: day.sunsetTime,
      windSpeed: day.windSpeed,
      pressure: day.pressure,
    }
  });
}

export function formatDaily(daily) {
  return Object.assign({}, daily, {
      icon: ICONS[getSvgIconName(daily.icon)],
    summary: daily.summary,
    updatedDateTime: formatTime(daily.updatedDateTime),
    data: formatDay(daily.data)
  });
}

export function formatData(forecast) {
  return _.pick(forecast, ['timezone', 'loading', 'error', 'allowed']);
}
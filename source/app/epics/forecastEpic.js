import { Observable } from 'rxjs';
import { combineEpics } from 'redux-observable';
import * as ActionTypes from '../actions/ActionsTypes';
import { fetchForecastData, setForecastError, loadGeoPosition, setGeoPosition, getForeCastData } from '../actions/ForecastActions';
import { onForecastData, getSocketConnection } from '../actions/ApplicationActions';

function loadForecastData(action$) {
  return action$.ofType(ActionTypes.GET_FORECAST_DATA)
    .mergeMap((action) =>
        Observable.from(fetchForecastData(action.payload))
          .map((response) => onForecastData(response.data))
          .catch(setForecastError));
}

function getGeoLocation(action$, store) {
  return action$.ofType(ActionTypes.GET_GEO_POSITION)
    .mergeMap(() =>
      Observable.from(loadGeoPosition())
        .map((response) => {
          store.dispatch(getForeCastData(response.location));
          store.dispatch(setGeoPosition(response));
          return getSocketConnection(response.location);
        })
        .catch(err => console.log(err))
    );
}


export default combineEpics(loadForecastData, getGeoLocation);
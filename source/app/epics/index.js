import { combineEpics, createEpicMiddleware } from 'redux-observable';
import updateEpic from './updateEpic';
import forecastEpic from './forecastEpic';

const rootEpic = (action$, store) =>
  combineEpics(updateEpic, forecastEpic)(action$, store)
    .catch((error, stream) => {
      return stream;
    });

export default createEpicMiddleware(rootEpic);

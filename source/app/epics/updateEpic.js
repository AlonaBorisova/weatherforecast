import { combineEpics } from 'redux-observable';
import openSocket from 'socket.io-client';
import { Observable } from 'rxjs';
import config from '../../../config/index';
import * as ActionTypes from '../actions/ActionsTypes';
import { onForecastData, setSocketConnection, getSocketConnection, closeSocketConnection } from '../actions/ApplicationActions';

export const reconnect = (store) => {
  store.dispatch(closeSocketConnection());
  store.dispatch(getSocketConnection());
};

const SocketUpdater = (action$, store) => (
  action$.ofType(ActionTypes.GET_SOCKET_CONNECTION)
    .switchMap((action) => {
      return new Observable(observer => {
        const socket = openSocket(`http://${config.server.host}:${config.server.port}`);
        store.dispatch(setSocketConnection(socket));

        socket.on('weather', (responce) => {
          if (responce.daily) {
            return observer.next(responce);
          }
          return null;
        });

        socket.emit('forecast', {
          interval: 15*60*1000,
          position: {
            latitude: action.payload.latitude,
            longitude: action.payload.longitude,
          }
        });

        socket.on('error', (err) => {
          console.log('error', new Date(), err);
        });
        socket.on('disconnect', () => {
          store.dispatch(closeSocketConnection());
          console.log('close', new Date());
        });
      })
    })
    .map(onForecastData)
    .catch(() => reconnect(store))
);

const stopSocketUpdater = (action$, store) => (
  action$.ofType(ActionTypes.CLOSE_SOCKET_CONNECTION)
    .map(() => {
      const socket = store.getState().application.socket;
      if (socket && socket.disconnected) {
        socket.disconnect();
      }

      return setSocketConnection(null);
    })
);

export default combineEpics(SocketUpdater, stopSocketUpdater);
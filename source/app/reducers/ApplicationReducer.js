import * as ActionTypes from '../actions/ActionsTypes';

const defaultState = {
  socket: null,
};

export default function Application(state = defaultState, action = {}) {
  switch (action.type) {
    case ActionTypes.SET_SOCKET_CONNECTION:
      return Object.assign({}, state, {
        socket: action.payload
      });
    default:
      return state;
  }
}
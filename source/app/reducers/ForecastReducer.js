import * as ActionTypes from '../actions/ActionsTypes';

const defaultState = {
  latitude: null,
  longitude: null,
  timezone: '',
  daily: {data: []},
  loading: false,
  error: false,
  allowed: false
};

export default function Forecast(state = defaultState, action = {}) {
  switch (action.type) {
    case ActionTypes.GET_FORECAST_DATA : {
      return Object.assign({}, state, {
        loading: true,
      });
    }
    case ActionTypes.SET_FORECAST_DATA : {
      return Object.assign({}, state, {
        latitude: action.payload.latitude,
        longitude: action.payload.longitude,
        timezone: action.payload.timezone,
        daily: action.payload.daily,
        loading: false,
        error: false,
      });
    }
    case ActionTypes.FETCH_FORECAST_ERROR : {
      return Object.assign({}, state, {
        loading: false,
        error: true,
      });
    }
    case ActionTypes.SET_GEO_POSITION:
      return Object.assign({}, state, {
        latitude: action.payload.location.latitude,
        longitude: action.payload.location.longitude,
        allowed: action.payload.allowed
      });
    case ActionTypes.CLOSE_SOCKET_CONNECTION: {
      return Object.assign({}, state, defaultState);
    }
    default:
      return state;
  }
}
import { combineReducers } from 'redux'
import application from './ApplicationReducer';
import forecast from './ForecastReducer';

const appReducer = combineReducers({
    application,
    forecast
  });

export default appReducer;
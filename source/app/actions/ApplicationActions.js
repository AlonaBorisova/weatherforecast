import * as ActionTypes from './ActionsTypes';

export function getSocketConnection(payload) {
  return {
    type: ActionTypes.GET_SOCKET_CONNECTION,
    payload,
  };
}

export function closeSocketConnection(payload) {
  return {
    type: ActionTypes.CLOSE_SOCKET_CONNECTION,
    payload,
  };
}

export function setSocketConnection(payload) {
  return {
    type: ActionTypes.SET_SOCKET_CONNECTION,
    payload,
  };
}

export function onForecastData(payload) {
  return {
    type: ActionTypes.SET_FORECAST_DATA,
    payload,
  };
}

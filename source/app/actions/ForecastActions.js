import axios from 'axios';
import * as ActionTypes from './ActionsTypes';
import DarkSkyApi from '../../../shared/DarkSkyWrapper';

export function getForeCastData(payload) {
  return {
    type: ActionTypes.GET_FORECAST_DATA,
    payload,
  };
}

export function fetchForecastData(payload) {
  return axios.get(`/forecast?longitude=${payload.longitude}&latitude=${payload.latitude}`);
}

export function setForecastError() {
  return {
    type: ActionTypes.FETCH_FORECAST_ERROR,
  };
}

export function loadGeoPosition() {
  return DarkSkyApi.loadCurrentPosition();
}


export function setGeoPosition(payload) {
  return {
    type: ActionTypes.SET_GEO_POSITION,
    payload,
  };
}

export function getGeoPosition(payload) {
  return {
    type: ActionTypes.GET_GEO_POSITION,
    payload,
  };
}

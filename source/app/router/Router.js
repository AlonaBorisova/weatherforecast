import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import routes from './Routes';
import App from '../containers/App';
import NotFound from '../components/NotFound';

export default () => (
  <Router>
    <App>
      <Switch>
        <Route {...routes.Home} />
        <Route component={NotFound} />
      </Switch>
    </App>
  </Router>
);

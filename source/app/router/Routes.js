import HomePageComponent from '../containers/Home';

export default {
  Home: {
    path: '/',
    component: HomePageComponent,
    exact: true,
  },
};
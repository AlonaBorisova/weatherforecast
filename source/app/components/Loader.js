import React from 'react';
import PropTypes from 'proptypes';

const Loader = (props) => {
  return (
    <div className="loader">{props.loading}</div>
  );
};

Loader.propTypes = {
  loading: PropTypes.string,
};

export default Loader;
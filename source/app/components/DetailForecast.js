import React, { Component } from 'react';
import PropTypes from 'proptypes';

const DetailForecast = (props) => {
  return (
    <div className="detailed-wrapper">
      <div>
        {props.content.apparent_temperature}
      </div>
      <div className="temperature">
        <span>{props.content.min} {props.weather.apparentTemperatureLow}</span><span>{props.content.max} {props.weather.apparentTemperatureHigh}</span>
      </div>
      <div>
        <span>{props.content.pressure}</span> {props.weather.pressure}
      </div>
      <div>
        <span>{props.content.humidity}</span> {props.weather.humidity}
      </div>
      <div>
        <span>{props.content.wind}</span>{props.weather.windSpeed}
      </div>
      <div>
        <span>{props.content.cloud_cover}</span> {props.weather.cloudCover}
      </div>
      <div>
        <span>{props.content.sunrise} {props.weather.sunriseTime}</span>
        <span>{props.content.sunset} {props.weather.sunsetTime}</span>
      </div>
    </div>
  );
};



DetailForecast.propTypes = {
  weather: PropTypes.shape({}),
  content: PropTypes.object
};


export default DetailForecast;

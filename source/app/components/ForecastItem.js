import React, { Component } from 'react';
import PropTypes from 'proptypes';
import Icon from '../components/Icon';
import { ICONS } from '../helpers/Icons';
import DetailForecast from '../components/DetailForecast';

class ForecastItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      extended: false
    };
    this.selectDay = this.selectDay.bind(this);
  }
  selectDay() {
   this.setState({ extended: !this.state.extended });
  }

  render() {
    const { weather } = this.props;
    const { content } = this.context;
    return (
      <div className="day-wrapper">
        <h3>{weather.day.toUpperCase()}</h3>
        <p>{weather.dateTime}</p>
        {!this.state.extended && <Icon icon={weather.icon} />}
        <div className="temperature">
          <span>{content.min} <span>{weather.temperatureLow}</span></span>
          <span>{content.max} <span>{weather.temperatureHigh}</span></span>
        </div>
        {!this.state.extended && <div className="day-summary">{weather.summary}</div>}
        {this.state.extended && <DetailForecast weather={weather} content={content} />}
          <div onClick={this.selectDay} className="details">{this.state.extended ? content.back : content.details}</div>
      </div>
    );
  }
}

ForecastItem.contextTypes = {
  content: PropTypes.object
};

ForecastItem.propTypes = {
  weather: PropTypes.shape({}),
};


export default ForecastItem;
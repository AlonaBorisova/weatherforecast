import React, { Component } from 'react';
import PropTypes from 'proptypes';

const styles = {
  container: {
    gridColumn: '1 / -1',
    background: '#0046b1',
    borderRadius: '5px',
    border: '1px solid #0046b1',
    color: '#fff',
    fontSize: '25px',
    padding: '15px',
    margin: '20px 10px',
    textAlign: 'center',
  },
};

class NotificationMessage extends Component {
  shouldComponentUpdate(nextProps) {
    return this.props.children !== nextProps.children;
  }
  render() {
    return (
      <div style={styles.container}>
        {this.props.children}
      </div>
    );
  }
}


NotificationMessage.propTypes = {
  children: PropTypes.any,
};


export default NotificationMessage;

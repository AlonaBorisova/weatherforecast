import React, { Component } from 'react';
import PropTypes from 'proptypes';
import { Style } from 'radium';
import '../../css/App.css';
import forecastStyles from '../../css/forecastStyles';
import contentUk from '../../locale/uk.json';

class App extends Component {
  getChildContext() {
    return { content: contentUk }
  }

  render() {
    return (
      <div className="app">
        <Style scopeSelector=".app" rules={forecastStyles} />
        {this.props.children}
      </div>
    );
  }
}
App.childContextTypes = {
  content: PropTypes.object,
};

App.propTypes = {
  children: PropTypes.element,
};


export default App;

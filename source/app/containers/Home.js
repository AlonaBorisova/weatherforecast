import  React, { Component } from 'react';
import PropTypes from 'proptypes';
import { connect } from 'react-redux';
import _ from 'lodash';
import ForecastItem from '../components/ForecastItem';
import NotificationMessage from '../components/NotificationMessage';
import Icon from '../components/Icon';
import forecastSelector from '../selectors/ForecastSelector';
import Loader from '../components/Loader';
import { getGeoPosition } from '../actions/ForecastActions';

class Home extends Component {
  componentDidMount() {
    this.props.getGeoPosition();
  }

  shouldComponentUpdate(nextProps) {
    return !_.isEqual(this.props, nextProps);
  }

  renderList() {
    const { daily } = this.props.forecast;

    return daily.data.reduce((children, item, index) => {
      children.push(
        <ForecastItem
          key={`${index}`}
          weather={item}
        />
      );
      return children;
    }, []);
  }

  render() {
    const { content } = this.context;
    const { error, loading, timezone, allowed } = this.props.forecast.generalInfo;

    if(error || !this.props.app.socket) {
      return (
        <NotificationMessage>
          {content.no_connection}
        </NotificationMessage>
      );
    }
    if (loading || !timezone) {
     return  <Loader loading={content.loading} />;
    }

    const { daily } = this.props.forecast;

    return (
      <div className="cards-container">
        {!allowed && timezone && (
          <NotificationMessage>
            {content.blocked_geo_location}
          </NotificationMessage>
        )}
        {!loading && daily.summary && (
          <h2 className="summary">
            <p>{content.your_location}</p>
            <p>{daily.summary}</p>
            <Icon icon={daily.icon} />
            <span>{content.update_time} : {daily.updatedDateTime}</span>
          </h2>)}
        {!loading && daily.data.length > 0 && this.renderList()}
      </div>
    );
  }
}

Home.contextTypes = {
  content: PropTypes.object,
};

Home.propTypes = {
  forecast: PropTypes.object,
  app: PropTypes.object,
};

const selector = forecastSelector();
const mapStateToProps = (state) => ({
  forecast: selector(state),
  app: state.application
});
export default connect(mapStateToProps, { getGeoPosition })(Home);

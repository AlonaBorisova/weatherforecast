const express = require('express');
const path = require('path');
const app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server);
const config = require('../config').server;
const DarkSkyApi = require('../shared/DarkSkyWrapper');

app.use(express.static(__dirname + '/../dist'));

let interval;
io.on('connection', (client) => {
  client.emit('connect', client);

  client.on('forecast', (param) => {
    if (interval) {
      clearInterval(interval);
    }

    interval = setInterval(() =>
      DarkSkyApi.getWeatherForecast(param.position, (result) => client.emit("weather", result)),
      param.interval);
  });

  client.on('disconnect', () => {
    console.log('client is disconnected');
  });
});

app.get('/forecast', (req, res) => {
  DarkSkyApi.getWeatherForecast({
    latitude: req.query.latitude,
    longitude: req.query.longitude,
  }, (result) => res.status(200).send(result));
});

app.get('/*', function (req, res) {
  res.sendFile(path.resolve(__dirname, '../dist/index.html'));
});

server.listen(config.port, () => console.log('listening on port ', config.port));


process.on('uncaughtException', (err) => {
  console.error((new Date).toUTCString() + ' uncaughtException:', err.message);
  console.error(err.stack);
  process.exit(1);
});
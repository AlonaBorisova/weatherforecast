const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const config = {
  entry: path.resolve(__dirname, './source/app/index.js'),
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'bundle.js',
  },
  watch: true,
  module: {
    loaders: [
      {
        test: /\.js?/,
        include: path.join(__dirname, './source/app'),
        exclude: /node_modules/,
        loader: 'babel-loader',
        query: {
          presets: ["env", "stage-3", "react"],
        },
      },
      {
        test: /\.css?/,
        include: path.join(__dirname, './source/css'),
        use: ['style-loader', 'css-loader']
      },
      {
        test: /\.(jpe?g|png|gif|svg)$/i,
        use: [
          {
            loader: 'url-loader',
            options: { limit: 10000, fallback: 'file-loader' },
          },
          {
            loader: 'img-loader',
            options: { progressive: true },
          },
        ],
      },
      {
        test: /\.txt$/,
        loader: 'raw-loader',
      },
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: 'source/index.html',
      inject: 'body',
      title: 'Weather Forecast',
      favicon: 'source/favicon.ico'
    })
  ]
};


module.exports = config;